class Funds {
  constructor(tokens) {
    this.tokens = tokens
  }

  withdraw: function(tokens, reciever) {
    if (this.tokens < tokens) {
      console.log('insufficient funds, aborting transaction')
      return
    }

    reciever.deposit(tokens)
  }

  deposit: function(tokens) {
    this.tokens += tokens
  }
}
