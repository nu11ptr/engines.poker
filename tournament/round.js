class Round {
  constructor(players, deck) {
    this.players = players
    this.deck    = deck
  }

  start: function() {
    this.players.forEach((player) => {
      player.hand = deck.draw(2)
    })
  }


}
